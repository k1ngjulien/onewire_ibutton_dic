/*
 * onewire.h
 * Peterka, Kandlhofer
 * March 2019
 * 
 * Library to interact with OneWire-Devices
 * Provides the most commonly used functonality and some usefull
 * utility-functions
*/

#include <stm32f10x.h>
#include "armv10_std.h"

#define u64 uint64_t
#define u32 uint32_t
#define u8  uint8_t

// Bitbanding for IDR of Bus
#define DATA_IN *((volatile unsigned long *)(BITBAND_PERI(GPIOA_IDR, 7)))
// Bitbanding for ODR of Bus
#define DATA_OUT *((volatile unsigned long *)(BITBAND_PERI(GPIOA_ODR, 7)))

// Standard OneWire ROM-Commands
enum Commands
{
	READ_ROM            = 0x33,
	READ_ROM_ALT        = 0x0F,
	SKIP_ROM            = 0xCC,
	MATCH_ROM           = 0x55,
	SEARCH_ROM          = 0xF0,
	OVERDRIVE_SKIP_ROM  = 0x3C,
	OVERDRIVE_MATCH_ROM = 0x69
};

// Initializes I/O Port PA7 for OneWire
void initOneWirePorts(void);

// Resets Bus and returns if device is present.
// return ... true or false
u8 resetBus(void);

// Sends 1 or 0 over Bus.
// value ... true or false, bit to be sent
void sendBit(u8 value);

// Reads bit from Bus ad returns it.
// return ... true or false, received bit
u8 readBit(void);


// Sends 8 Bits over Bus.
// data ... byte to send
void sendByte(u8 data);

// Reads 8 Bits from Bus.
// return ... received byte
u8 readByte(void);

// Find devices on OneWire bus
// addresses    ... passed by reference, array where addresses will be stored
// max_devices 	... length of addresses array
// return       ... >= 0, number of found devices
u8 findDevices (u64 addresses[], u8 max_devices);

// Calculate CRC of data
// data   ... value to calculate crc of
// return ... CRC
u8 calculate_crc (u64 data);

// Utility:

// Wait for 10us * x
// x ... number of 10us to wait
void wait_10xus(int x);

// Set Bit at position
// number   ... passed by reference, number to set bit in
// position ... >= 0, position of bit to be set
// value 		... true or false, value of bit
void setBit(u64 *number, u8 position, u8 value);

// Print 64bits as hexadecimal
// val ... 64bit value
void uart_put_64hex(u64 val);

// Get value of single bit
// value  ... source value
// number ... index of bit to get
// return ... true or false, value of bit
u8 getBitNumber(u64 value, u8 number);

// Get value of specific byte
// value  ... source value
// number ... index of byte to get
// return ... byte
u8 getByteNumber(u64 value, u8 number);
