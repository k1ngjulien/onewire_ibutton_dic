#include <stm32f10x.h>
#include "armv10_std.h"
#include "onewire.h"

int main()
{
	initOneWirePorts();
	uart_init(9600); // 86400 baud @ 72MHz
	uart_put_string("\r\n");
	
	int num_devices = 0;
	u64 addresses[5];
	
	// check if there are devices on the bus
	if (resetBus())
	{
		num_devices = findDevices(addresses, 5);
		
		// print the address of all found devices
		for(int i = 0; i < num_devices; i++) {
			uart_put_string("Device ");
			uart_put_hex(i);
			uart_put_string(": ");
			uart_put_64hex(addresses[i]);
			uart_put_string(" CRC: ");
			uart_put_hex(calculate_crc(addresses[i]));
			uart_put_string("\r\n");
		}
	}
	
	uart_put_string("\r\n");

	while (1)
	{
	}
	return 0;
}


