#include "onewire.h"

void initOneWirePorts(void)
{
	// Enable GPIO Clock
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;

	// Init PA7 as GP Push Pull
	int tmp = GPIOA->CRL;
	tmp &= 0x0FFFFFFF;
	tmp |= 0x30000000;
	GPIOA->CRL = tmp;
}

void sendBit(u8 value)
{
	if (value)
	{
		DATA_OUT = 0;
		wait_10xus(1);
		DATA_OUT = 1;
		wait_10xus(6);
	}
	else
	{
		DATA_OUT = 0;
		wait_10xus(6);
		DATA_OUT = 1;
		wait_10xus(1);
	}
}

u8 readBit(void)
{
	u8 value = 0;
	DATA_OUT = 0;
	wait_10xus(1);
	DATA_OUT = 1;
	value = DATA_IN;
	wait_10xus(6);
	return value;
}

u8 resetBus(void)
{
	u8 isDevicePresent = 0;
	
	DATA_OUT = 0;
	wait_10xus(45); 	

	DATA_OUT = 1;
	wait_10xus(6);

	isDevicePresent = ~DATA_IN;
	
	wait_10xus(50);

	return isDevicePresent;
}

void sendByte(u8 data)
{
	for (u8 i = 0; i < 8; i++)
	{
		sendBit(data & 0x01); // Send LSB
		data >>= 1;           // Shift right for next Bit
	}
}

u8 readByte(void)
{
	u8 value = 0;

	// read LSB to MSB
	for (u8 i = 0; i < 8; i++)
	{
		value >>= 1;
		if (readBit())
			value |= 0x80;
	}
	return value;
}

// Guide:
// https://www.maximintegrated.com/en/app-notes/index.mvp/id/187	
// Changes:
// id_bit_number starts at 0, not 1
u8 findDevices (u64 addresses[], u8 max_devices)
{
	u64 rom_no;
	u8 search_direction;
	u8 id_bit, cmp_id_bit;
	u8 presence;
	u8 current_device           = 0;
	u8 id_bit_number            = 0;
	u8 last_zero                = 0;
	u8 last_device_flag         = 0;
	u8 last_discrepancy         = 0;
	u8 last_family_discrepancy 	= 0;

	do {
		// Check if there are devices on the bus
		presence = resetBus();
		if(!presence) {
			break;
		}
		
		// Initial Condition
		id_bit_number = 0;
		last_zero = 0;
		sendByte(SEARCH_ROM);
		do {
			id_bit = readBit();
			cmp_id_bit = readBit();
				
			// No devices on the bus or error
			if((id_bit == 1)&&(cmp_id_bit == 1)){
				break;
			}
			
			// Both 0 and 1 at this bit?
			if((id_bit == 0)&&(cmp_id_bit == 0)){
				
				// At the last discrepancy point?
				if(id_bit_number == last_discrepancy){
					// go 1 direction
					search_direction = 1;
				}
				// are we already after the discrepancy
				else if(id_bit_number > last_discrepancy){
					// go 0 direction
					search_direction = 0;
				}
				// no we are in front of the last discrepancy
				// send the same bit as last time
				else {			
					search_direction = getBitNumber(rom_no, id_bit_number);
				}
				
				// should we go to 0?
				if(search_direction == 0){
					last_zero = id_bit_number;
					
					// are we still in the family code?
					if(last_zero < 9){
						// move along last family discrepancy
						last_family_discrepancy = last_zero;
					}
				}
			}
			// No, bits are the same on all devices here
			else{
				// go in the direction of the bit we just got
				search_direction = id_bit;	
			}
			
			// store bit in rom_no
			setBit(&rom_no, id_bit_number, search_direction);
			// and send it to go this path
			sendBit(search_direction);
			
			// next bit
			id_bit_number++;
		}
		while(id_bit_number < 64);
		
		// Check if last device on the bus
		last_discrepancy = last_zero;
		if(last_discrepancy == 0){
			last_device_flag = 1;
		}
		
		//is crc correct?
		if(calculate_crc(rom_no) != getByteNumber(rom_no,7)){
			break;
		}
		
		// Store address in array
		addresses[current_device] = rom_no;
		current_device++;
	}
	while(last_device_flag != 1 && current_device < max_devices);
	
	return current_device;
}


// Guide:
// Book of iButton Standards, Page 128-131
u8 calculate_crc (u64 data)
{
	// Lookuptable for calculating the CRC
	const u8 crc_lut[256] = {
        0, 94,188,226, 97, 63,221,131,194,156,126, 32,163,253, 31, 65,
		157,195, 33,127,252,162, 64, 30, 95,  1,227,189, 62, 96,130,220,
		 35,125,159,193, 66, 28,254,160,225,191, 93,  3,128,222, 60, 98,
		190,224,  2, 92,223,129, 99, 61,124, 34,192,158, 29, 67,161,255,
		 70, 24,250,164, 39,121,155,197,132,218, 56,102,229,187, 89,  7,
		219,133,103, 57,186,228,  6, 88, 25, 71,165,251,120, 38,196,154,
		101, 59,217,135,  4, 90,184,230,167,249, 27, 69,198,152,122, 36,
		248,166, 68, 26,153,199, 37,123, 58,100,134,216, 91,  5,231,185,
		140,210, 48,110,237,179, 81, 15, 78, 16,242,172, 47,113,147,205,
		 17, 79,173,243,112, 46,204,146,211,141,111, 49,178,236, 14, 80,
		175,241, 19, 77,206,144,114, 44,109, 51,209,143, 12, 82,176,238,
		 50,108,142,208, 83, 13,239,177,240,174, 76, 18,145,207, 45,115,
		202,148,118, 40,171,245, 23, 73,  8, 86,180,234,105, 55,213,139,
		 87,  9,235,181, 54,104,138,212,149,203, 41,119,244,170, 72, 22,
		233,183, 85, 11,136,214, 52,106, 43,117,151,201, 74, 20,246,168,
		116, 42,200,150, 21, 75,169,247,182,232, 10, 84,215,137,107, 53
	};
	
	u8 result=0;
	
	for(u8 i=0;i<=6;i++)
	{
		// calculate crc of familycode + serial number
		result= crc_lut[ result ^ getByteNumber(data, i) ];		
	}
	// return crc
	return result;
}

void setBit(u64 *number, u8 position, u8 value){
	if(value == 1)
		*number |= 1ULL << position;
	else if(value == 0)
		*number &= ~(1ULL << position);
}

u8 getBitNumber(u64 value, u8 number) {
	return (value >> number) & 0x1;
}

u8 getByteNumber(u64 value, u8 number) {
	return (value >> (number * 8)) & 0xFF;
}

void uart_put_64hex(u64 val) {
	// MSB to LSB
	u8 output = 0;
	for (int i = 0; i < 8; i++)
	{
		output = (val>>56) & 0xFF;
		val <<= 8;
		uart_put_hex(output);
	}
}

void wait_10xus(int x)
{
	u32 i, j;
	for (i = 0; i < x; i++)
	{
		for (j = 0; j < 107; j++)
		{
		}
	}
}
